--------------------------------------------------------------------------------
-- Copyright (c) 2009 Flimgoblin <fingoniel@roguishness.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
-- File:      MiniMapMonster.lua
-- Date:      2009-04-11T15:41:00Z
-- Author:    Flimgoblin <fingoniel@roguishness.com>
-- Version:   Beta v0.1
-- Copyright: 2009
--------------------------------------------------------------------------------
-- MiniMapMonster main file
-- Shows MapMonster pins on your MiniMap
-- Requires MapMonster to run.
-- Uses the MapMonsterAPI from: Bloodwalker <metagamegeek@gmail.com>
--------------------------------------------------------------------------------
local pairs = pairs

MiniMapMonster = {			
	--lastProcessed=0,

	--MINI_MAP_WINDOW="EA_Window_OverheadMapMapDisplay",
	mapZoneId=0,
	WINDOW_SCALE={40.93, 29,19.5},
	DEFAULT_ICON_SCALE=0.8,
	pinDisplay={},
	windowHooks={},
	pinsShowing = true	-- for Minimaps that support ZoneView
}


local function pm(msg)	
	if ( not msg ) then
		return
	else
		msg=tostring(msg)
		EA_ChatWindow.Print(towstring("[MiniMapMonster]: "..msg))
	end
end

local round=function(num,idp)
	return tonumber(string.format("%." .. (idp or 0) .. "f", num))
end

local function DisplayPin(pinId,winx,winy)
	local pin=MapMonsterAPI.GetPin(pinId)
	if(not pin) then return end
	MiniMapMonster.pinDisplay[pin.id]={id=pin.id,x=winx,y=winy}	
	
	local newMapIconName = "MiniMapMonster_MapPin" .. pin.id
	local pinType=MapMonsterAPI.GetPinTypeOptions(pin.pinType, pin.subType)

	local mapIcon = pinType.mapIcon
	if(not MiniMapMonster.CurrentMiniMap or not DoesWindowExist(MiniMapMonster.CurrentMiniMap)) then MiniMapMonster.DetectMiniMap() end
	
	CreateWindowFromTemplate(newMapIconName, "MiniMapMonster_MapPin",MiniMapMonster.CurrentMiniMap)

	DynamicImageSetTexture(newMapIconName, mapIcon.texture, mapIcon.x or 0, mapIcon.y or 0)
	DynamicImageSetTextureScale(newMapIconName, mapIcon.scale) 
	if mapIcon.slice then DynamicImageSetTextureSlice(newMapIconName, mapIcon.slice) end
	if mapIcon.tint then
		WindowSetTintColor(newMapIconName, mapIcon.tint.r, mapIcon.tint.g, mapIcon.tint.b)
	else
		WindowSetTintColor(newMapIconName, 255, 255, 255)
	end
	WindowSetId(newMapIconName,pin.id)
	WindowSetScale(newMapIconName,MiniMapMonster_Config.iconScale* WindowGetScale(MiniMapMonster.CurrentMiniMap))	--WindowGetScale("EA_Window_OverheadMapMapDisplay"))
	WindowClearAnchors(newMapIconName)
	WindowAddAnchor(newMapIconName, "center", MiniMapMonster.CurrentMiniMap, "center", winx, winy)

	if MiniMapMonster.pinsShowing then
		WindowSetShowing(newMapIconName, true)
	else
		WindowSetShowing(newMapIconName, false)
	end
end

function MiniMapMonster.RemovePin(pinId)
	if(MiniMapMonster.pinDisplay[pinId]) then
		if(DoesWindowExist("MiniMapMonster_MapPin"..pinId)) then DestroyWindow("MiniMapMonster_MapPin"..pinId) end
		MiniMapMonster.pinDisplay[pinId]=false
	end 
end

function MiniMapMonster.RemoveAllPins()
	for pinId,_ in pairs(MiniMapMonster.pinDisplay) do
		MiniMapMonster.RemovePin(pinId)
	end
end

local function MovePin(pinId,winx,winy)
	if not MiniMapMonster.pinsShowing then return end
	local last=MiniMapMonster.pinDisplay[pinId]
	if(not last) then
		last={id=pinId,x=winx,y=winy}
		MiniMapMonster.pinDisplay[pinId]=last
	elseif (last.x==winx and last.y==winy) then
		return
	end
	
	if(not MiniMapMonster.CurrentMiniMap or not DoesWindowExist(MiniMapMonster.CurrentMiniMap)) then MiniMapMonster.DetectMiniMap() end
	
	local win="MiniMapMonster_MapPin"..pinId
	WindowSetShowing(win,true)
	WindowClearAnchors(win)
	WindowSetScale(win, MiniMapMonster_Config.iconScale * WindowGetScale(MiniMapMonster.CurrentMiniMap))	--WindowGetScale("EA_Window_OverheadMapMapDisplay"))
	WindowAddAnchor(win,"center",MiniMapMonster.CurrentMiniMap,"center",winx,winy)
end

--
---- MapMonster Hook
--
function MiniMapMonster.CreatePin(...)
	local pinId=MiniMapMonster.hookCreatePin(...)
	MiniMapMonster.UpdatePins()
	return pinId
end

function MiniMapMonster.DeletePin(pinId)	
	local success=MiniMapMonster.hookDeletePin(pinId)
	if(success)	then MiniMapMonster.RemovePin(pinId) end
	return success
end
--
---- /MapMonster Hook
--

function MiniMapMonster.Init()		
	RegisterEventHandler(SystemData.Events.PLAYER_ZONE_CHANGED, "MiniMapMonster.OnPlayerZoneChanged")
	RegisterEventHandler(SystemData.Events.PLAYER_POSITION_UPDATED, "MiniMapMonster.OnPlayerPositionUpdated")
	
	MiniMapMonster.hookCreatePin=MapMonsterAPI.CreatePin
	MapMonsterAPI.CreatePin=MiniMapMonster.CreatePin	
	MiniMapMonster.hookDeletePin=MapMonsterAPI.DeletePin
	MapMonsterAPI.DeletePin=MiniMapMonster.DeletePin
	
	if(not MiniMapMonster_Config) then
		MiniMapMonster_Config={}
		MiniMapMonster_Config.iconScale=MiniMapMonster.DEFAULT_ICON_SCALE
	end	

	pm("loaded")
end

function MiniMapMonster.SetIconScale(i) MiniMapMonster_Config.iconScale=i end

function MiniMapMonster.OnPlayerPositionUpdated() MiniMapMonster.UpdatePins() end
function MiniMapMonster.OnPlayerZoneChanged() MiniMapMonster.RemoveAllPins() end

function MiniMapMonster.UpdatePins()
	if (not MiniMapMonster.CurrentMiniMap or not DoesWindowExist(MiniMapMonster.CurrentMiniMap)) then MiniMapMonster.DetectMiniMap() end

	local pos=MapMonsterAPI.GetPlayerPosition()
	if(pos and not pos.zoneId==MiniMapMonster.mapZoneId) then MiniMapMonster.RemoveAllPins() end
	if not (pos and pos.worldX) then return end
	
	MiniMapMonster.mapZoneId=pos.zoneId
	
	local zoneInfo = MapMonsterAPI.GetZoneData(pos.zoneId)
	if(not zoneInfo or zoneInfo.noMap) then return false end
	
	local zoom=GetOverheadMapZoomLevel()
	local pins=MapMonsterAPI.GetPinsForZone(pos.zoneId)
	
	if(pins) then
		local width, height = WindowGetDimensions(MiniMapMonster.CurrentMiniMap)
		width = width - 10
		height = height - 10
		for pinId,pin in pairs(pins) do

			local winx = (pin.worldX-pos.worldX) / MiniMapMonster.WINDOW_SCALE[zoom+1]
			local winy = (pin.worldY-pos.worldY) / MiniMapMonster.WINDOW_SCALE[zoom+1]			

			if(winx>=-width/2 and winx<=width/2 and winy>=-height/2 and winy<=height/2) then			
				if(not DoesWindowExist("MiniMapMonster_MapPin"..pin.id)) then
					DisplayPin(pin.id,winx,winy)
				else
					MovePin(pin.id,winx,winy)
				end												
			else 
				MiniMapMonster.RemovePin(pin.id)
			end
		end	
	end
	
	--local xPosUnit = windowX / zoneInfo.mapsizeX
	--local yPosUnit = windowY / zoneInfo.mapsizeY


-- 1000world = 45 (zoom 2)      
-- 1000world = 30 (zoom 1)
-- 1000world = 22 (zoom 0)

-- map is 90 radius	
--	local anchorX = pin.zoneX * xPosUnit
--	local anchorY = pin.zoneY * yPosUnit

--	WindowClearAnchors(newMapIconName)
--	WindowAddAnchor(newMapIconName, "topleft", "EA_Window_WorldMapZoneViewMapDisplay", "center", anchorX, anchorY)
end

function MiniMapMonster.OnMouseClickPin() end
function MiniMapMonster.OnMouseRightClickPin() end
function MiniMapMonster.OnMouseOverPin()
	local windowName	= SystemData.ActiveWindow.name
	
	local id=windowName:match("MiniMapMonster_MapPin([0-9]+)")
	if(id) then
		id=tonumber(id)
		local data=MapMonsterAPI.GetPin(id)		
		local msg=L"test"
	
		Tooltips.CreateTextOnlyTooltip (windowName, nil)
		Tooltips.SetTooltipText (1, 1, data.label)
		--Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
		Tooltips.Finalize()
		    
		local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
		Tooltips.AnchorTooltip (anchor)
		Tooltips.SetTooltipAlpha (1)
	end
end


function MiniMapMonster.SetMiniMapWindow(window)	
	MiniMapMonster.CurrentMiniMap=window	
	pm("Window changed to "..window) 
end

local MinmapSlideZoomHook
function MiniMapMonster.DetectMiniMap()
	if(CMapWindow) then
		MiniMapMonster.SetMiniMapWindow("CMapWindowMapDisplay")	
	elseif(Minmap) then
		if MiniMapMonster.CurrentMiniMap ~= "MinmapMapDisplay" then
			local function OnZoomChanged()
					if MinmapSettings.zoomlevel < 1.5 then

						for pinId,_ in pairs(MiniMapMonster.pinDisplay) do
							if not MiniMapMonster.pinsShowing then
								WindowSetShowing("MiniMapMonster_MapPin"..pinId, true)	-- show if hidden
							end
							MiniMapMonster.pinsShowing = true
						end
					else

						for pinId,_ in pairs(MiniMapMonster.pinDisplay) do
							WindowSetShowing("MiniMapMonster_MapPin"..pinId, false)	-- hide
						end
						MiniMapMonster.pinsShowing = false
					end
				end
			MinmapSlideZoomHook = Minmap.SlideZoom
			Minmap.SlideZoom = function(...)
					MinmapSlideZoomHook(...)
					OnZoomChanged()
				end
			OnZoomChanged()
		end
		MiniMapMonster.SetMiniMapWindow("MinmapMapDisplay")
	else
		MiniMapMonster.SetMiniMapWindow("EA_Window_OverheadMapMapDisplay")	
	end 
end