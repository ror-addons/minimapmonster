<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="MiniMapMonster" version="0.9" date="28/02/2011" >
		<Author name="Wothor; Flimgoblin" email="fingonielweb@roguishness.com" />
		<Description text="Put your MapMonster pins on your MiniMap" />
		<VersionSettings gameVersion="1.4.1" windowsVersion="0.1" savedVariablesVersion="0.4" />
	    <Dependencies>                      
             <Dependency name="MapMonster" /> 
             <Dependency name="CMap" optional="true" forceEnable="false"/>
             <Dependency name="Minmap" optional="true" forceEnable="false"/>
        </Dependencies>

		<Files>			
			<File name="MiniMapMonster.lua" />											
			<File name="MiniMapMonster.xml" />											
		</Files>
		<OnInitialize>
			<CallFunction name="MiniMapMonster.Init" />	
		</OnInitialize>		
		<OnUpdate>			
		</OnUpdate>		
		<SavedVariables>			
			<SavedVariable name="MiniMapMonster_Config" />
		</SavedVariables>
	</UiMod>
</ModuleFile>    